from matplotlib import pyplot as plt
import numpy as np
import matplotlib

fig, ax = plt.subplots()

def plateau():
      plt.axis('equal')    
      axes = plt.gca()
      axes.set_xlim(-1,16)
      axes.set_ylim(-1,16)


      plt.vlines(x=0, ymin=0, ymax=15)
      plt.vlines(x=5, ymin=0, ymax=15)
      plt.vlines(x=10, ymin=0, ymax=15)
      plt.vlines(x=15, ymin=0, ymax=15)

      plt.hlines(y=0, xmin=0, xmax=15)
      plt.hlines(y=5, xmin=0, xmax=15)
      plt.hlines(y=10, xmin=0, xmax=15)
      plt.hlines(y=15, xmin=0, xmax=15)

      plt.plot(0, 50,'rx-')

      plt.show()


def croix(x,y):
      axes = plt.gca()
      axes.add_artist(matplotlib.lines.Line2D((x-2,x+2), (y+2, y-2), color = 'red'))
      axes.add_artist(matplotlib.lines.Line2D((x+2,x-2), (y+2, y-2), color = 'red'))
      plt.show()
      
def rond(x,y):
      axes = plt.gca()
      axes.add_artist(matplotlib.patches.Circle((x, y), 2, color = 'Blue',fill = False)) 
      plt.show()


      

def onclick_croix(event):
      croix(event.xdata,event.ydata)

def onclick_rond(event):
      rond(event.xdata,event.ydata)



def jeu():
      plateau()
      fin = False
      cpt_tour = 1
      while fin != True: #tour
            print(cpt_tour)
            if cpt_tour%2 == 0 :
                  fig.canvas.mpl_connect('button_press_event', onclick_rond)
                  cpt_tour += 1
            else : 
                  fig.canvas.mpl_connect('button_press_event', onclick_croix)
                  cpt_tour += 1
            if cpt_tour == 4 :
                  fin = True





jeu()

